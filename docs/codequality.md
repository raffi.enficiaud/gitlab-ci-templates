# Code Quality templates

The Code Quality template file `codequality.yml` can be included via

```yaml
include:
  - project: computing/gitlab-ci-templates
    file: codequality.yml
```

This file provides the following job templates:

- [`.codequality:combine`](#.codequality:combine)
- [`.codequality:markdownlint`](#.codequality:markdownlint)

## `.codequality:combine` {: #.codequality:combine }

### Description {: #.codequality:cache-description }

Combines multiple
[Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)
reports into a single report, to work around the GitLab limitation
that
[only a single Code Quality report can be displayed](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#only-a-single-code-quality-report-is-displayed-but-more-are-defined).

The following variables can be used to configure the build:

| Name                            | Default    | Purpose                                    |
| ------------------------------- | ---------- | ------------------------------------------ |
| `CODEQUALITY_GLOB` {: .nowrap } | `"*.json"` | glob string to identify reports to combine |

### Example usage {: #.codequality:cache-example }

```yaml
include:
  - project: computing/gitlab-ci-templates
    file:
      - codequality.yml
      - python.yml

flake8:
  stage: analysis
  extends:
    - .python:flake8
  # upload flake8 report as artifact to be used by codequality
  artifacts:
    paths:
      - flake8.json

radon:
  stage: analysis
  extends:
    - .python:radon
  # upload radon report as artifact to be used by codequality
  artifacts:
    paths:
      - radon.json

codequality:
  stage: .post
  extends:
    - .codequality:combine
  needs:
    - flake8
    - radon
```

## `.codequality:markdownlint` {: #.codequality:markdownlint }

### Description {: #.codequality:markdownlint-description }

Runs [`markdownlint`](https://github.com/DavidAnson/markdownlint) over
a project/directory using
[`markdownlint-cli`](https://github.com/igorshubovych/markdownlint-cli).

The following variables can be used to configure the build:

| Name                                | Default             | Purpose |
| ----------------------------------- | ------------------- | ------- |
| `MARKDOWNLINT_OPTIONS` {: .nowrap } | `${CI_PROJECT_DIR}` | options to pass to `markdownlint-cli`, defaults to scanning the whole project |

### Example usage {: #.codequality:markdownlint-example }

```yaml
include:
  - project: computing/gitlab-ci-templates
    file:
      - codequality.yml

markdownlint:
  stage: analysis
  extends: .codequality:markdownlint
```

## `.codequality:shellcheck` {: #.codequality:shellcheck }

### Description {: #.codequality:shellcheck-description }

Runs [`shellcheck`](https://github.com/koalaman/shellcheck)
over one or more files in a project.

The following variables can be used to configure the build:

| Name                              | Default             | Purpose |
| --------------------------------- | ------------------- | ------- |
| `SHELLCHECK_TARGETS` {: .nowrap } | `"*.sh"`            | wildcard to pass to `find . -path <>` to locate files to check (see [manual](https://man7.org/linux/man-pages/man1/find.1.html) for details of `-path` syntax) |
| `SHELLCHECK_OPTIONS` {: .nowrap } | `${CI_PROJECT_DIR}` | options to pass to `shellcheck-cli`, defaults to scanning the whole project |

!!! tip "Jobs using this template will not fail when lint is found"

    Jobs that use this template will always pass when the linter executes
    successfully, regardless of what lint is found.

    Users should rely upon the Code Quality
    [merge request widget](https://git.ligo.org/help/ci/testing/code_quality.html#merge-request-widget)
    visible from the GitLab web interface.

### Example usage {: #.codequality:shellcheck-example }

```yaml
include:
  - project: computing/gitlab-ci-templates
    file:
      - codequality.yml

shellcheck:
  stage: analysis
  extends: .codequality:shellcheck
```
