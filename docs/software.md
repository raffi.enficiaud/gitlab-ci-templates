# Software templates

The Software template file `software.yml` can be included via:

```yaml
include:
  - project: computing/gitlab-ci-templates
    file: software.yml
```

This file provides the following job templates:

- [`.software:upload-source`](#.software:upload-source)

## `.software:upload-source` {: #.software:upload-source }

### Description {: #.software:upload-source-description }

This template uploads a source distribution (tarball) to the
[IGWN Source Archive (https://software.igwn.org)](
https://computing.docs.ligo.org/guide/software/distribution/#source).

With default options, this job approximately runs the following:

```bash
igwn-source-upload "${IGWN_ARCHIVE_TARGET}"
```

The job can be configured using the following variables:

| Name                                      | Default                  | Purpose |
| ----------------------------------------- | ------------------------ | ------- |
| `IGWN_ARCHIVE_HOST`                       | `"software.igwn.org"`    | The host to upload to. |
| `IGWN_ARCHIVE_USER`                       | `"${GITLAB_USER_LOGIN}"` | The user to connect as over SSH. This must be the username associated with the SSH private key. |
| `IGWN_ARCHIVE_RSYNC_MODULE`               | `"lscsrc"` | The remote Rsync module to upload to. |
| `IGWN_ARCHIVE_TARGET`                     | `"*.tar.*"`              | The path of the file to upload. |
| `IGWN_SOURCE_UPLOAD_OPTIONS` {: .nowrap } | `"--verbose --verbose"`  | Options to pass to `igwn-source-upload`. |

**Additionally** this job should be configured using the following
[protected, masked variables](https://git.ligo.org/help/ci/variables/index.html#cicd-variable-security):

| Name                                        | Purpose |
| ------------------------------------------- | ------- |
| `IGWN_ARCHIVE_RSYNC_PASSWORD`               | The password to use when communicating over `rsync`, see <https://secrets.ligo.org/secrets/433/> |
| `IGWN_ARCHIVE_SSH_PRIVATE_KEY` {: .nowrap } | The SSH private key (file or `base64` encoded string) to use when connecting to the archive, see [here](https://git.ligo.org/help/ci/ssh_keys/index.html#ssh-keys-when-using-the-docker-executor) |

!!! tip "Protected variables only work with protected branches or tags."

    Protected variables only work on protected branches or tags, so you must
    ensure that if you configure the CI/CD variables as 'Protected', you also
    configure the branch or tag you will push to to be protected.
    For more details, see
    <https://git.ligo.org/help/ci/variables/index.html#protect-a-cicd-variable>.

### Example usage {: #.cmake:pack-example }

```yaml
test:
  extends: .software:upload-source
  needs: [tarball]
```
