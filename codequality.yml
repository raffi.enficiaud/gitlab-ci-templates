#
# Templates for Code Quality
#

# -- Code Quality combine ---
# combine multiple code
# quality JSON reports
# ---------------------------

.codequality:combine:
  image: python:slim
  variables:
    CODEQUALITY_GLOB: "*.json"
  script:
    # list reports (sanity check)
    - ls ${CODEQUALITY_GLOB}
    # combine all individual reports
    - |
      python - << EOF
      import glob, json
      lines = []
      for jfile in glob.glob("${CODEQUALITY_GLOB}", recursive=True):
          print(f"Parsing {jfile}...")
          with open(jfile, "r") as file:
              lines.extend(json.load(file))
      outfile = "gl-code-quality-report.json"
      with open(outfile, "w") as out:
          json.dump(lines, out, separators=(",", ":"))
      print(f"{outfile} written")
      EOF
    # print the report
    - python -m json.tool gl-code-quality-report.json
  artifacts:
    reports:
      codequality: gl-code-quality-report.json

# -- Markdownlint -----------
# run markdownlint-cli
# ---------------------------

.codequality:markdownlint:
  image: igwn/lint
  variables:
    # by default just lint the whole project
    MARKDOWNLINT_OPTIONS: "${CI_PROJECT_DIR}"
  before_script:
    # install python3 to support post-processing the JSON
    - dnf -y -q install
          python3
    # print installed packages to aid debugging
    - npm list --global
    - markdownlint --version
  script:
    - set -- ${MARKDOWNLINT_OPTIONS}
    # run markdownlint once to print to the screen
    - markdownlint "$@" || true
    # run it again in JSON mode
    - markdownlint "$@" --json --output raw-markdownlint.json || true
    # and convert it to python
    - |
      python3 - <<EOF
      import json
      import hashlib
      import os
      from os.path import relpath

      # read raw markdownlint JSON
      with open("raw-markdownlint.json", "r") as file:
          raw = file.read().strip()
      if raw:
          lint = json.loads(raw)
      else:  # no issues
          lint = []
      issues = []

      # format each issue based on the codeclimate spec, see
      # https://git.ligo.org/help/ci/testing/code_quality.html#implementing-a-custom-tool
      for item in lint:
          desc = item["ruleDescription"]
          if item.get("errorDetail"):
              desc += ": " + item["errorDetail"]
          path = relpath(item["fileName"])
          try:
              line = open(path, "r").readlines()[item["lineNumber"]-1]
          except IndexError:  # error at end of file
              line = "END"
          issues.append({
              "categories": ["Style"],
              "check_name": item["ruleNames"][1],
              "description": desc,
              "content": {
                  "body": item["ruleInformation"],
              },
              "fingerprint": hashlib.sha1(" ".join([
                  item["ruleNames"][1],
                  path,
                  line.replace(" ", ""),
              ]).encode("utf-8")).hexdigest(),
              "location": {
                  "lines": {
                      "begin": int(item["lineNumber"]),
                      "end": int(item["lineNumber"]),
                  },
                  "path": path,
              },
              "type": "issue",
              "severity": "info",
          })

      # write output
      with open(f"markdownlint-{os.environ['CI_JOB_NAME_SLUG']}.json", "w") as file:
          json.dump(issues, file, separators=(',', ':'))
      EOF
  artifacts:
    when: always
    reports:
      codequality: markdownlint*.json

# -- ShellCheck -------------
# run shellcheck
# ---------------------------

.codequality:shellcheck:
  image: pipelinecomponents/shellcheck
  variables:
    # by default find all .sh files to lint
    SHELLCHECK_TARGETS: "*.sh"
    # other options to pass to shellcheck
    SHELLCHECK_OPTIONS: ""
  before_script:
    # install python3 to support post-processing the JSON
    - apk add -q
          python3
    # debugging
    - shellcheck --version
  script:
    # `find . -path <>` doesn't like relative paths without the ./ prefix, so add it
    - if [[ "${SHELLCHECK_TARGETS::1}" =~ [a-zA-Z0-9] ]]; then SHELLCHECK_TARGETS="./${SHELLCHECK_TARGETS}"; fi
    # find all files to check
    - TARGETS=$(find . -path "${SHELLCHECK_TARGETS}")
    # convert options variable to shell arguments
    - set -- ${SHELLCHECK_OPTIONS}
    # run shellcheck once to print to the screen
    - echo $ shellcheck "$@" ${TARGETS}
    - shellcheck "$@" ${TARGETS} || true
    # run it again in JSON mode
    - shellcheck "$@" ${TARGETS} --format json > raw-shellcheck.json || true
    # and convert it to python
    - |
      python3 - <<EOF
      import json
      import hashlib
      import os
      from os.path import relpath

      # read raw shellcheck JSON
      with open("raw-shellcheck.json", "r") as file:
          raw = file.read().strip()
      if raw:
          lint = json.loads(raw)
      else:  # no issues
          lint = []
      issues = []

      SEVERITY = {
          "error": "major",
          "warning": "minor",
          "style": "info",
      }

      # format each issue based on the codeclimate spec, see
      # https://git.ligo.org/help/ci/testing/code_quality.html#implementing-a-custom-tool
      for item in lint:
          path = relpath(item["file"])
          try:
              line = open(path, "r").readlines()[item["line"]-1]
          except IndexError:  # error at end of file
              line = "END"
          issues.append({
              "categories": ["Style"],
              "check_name": f"SC{item['code']}",
              "description": item["message"],
              "fingerprint": hashlib.sha1(" ".join([
                  str(item["code"]),
                  path,
                  line.replace(" ", ""),
              ]).encode("utf-8")).hexdigest(),
              "location": {
                  "lines": {
                      "begin": int(item["line"]),
                      "end": int(item["endLine"]),
                  },
                  "path": path,
              },
              "type": "issue",
              "severity": SEVERITY.get(item["level"], item["level"]),
          })

      # write output
      with open(f"shellcheck-{os.environ['CI_JOB_NAME_SLUG']}.json", "w") as file:
          json.dump(issues, file, separators=(',', ':'))
      EOF
  artifacts:
    when: always
    reports:
      codequality: shellcheck*.json
